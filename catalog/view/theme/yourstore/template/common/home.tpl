<?php
if (!isset($store_id)) {$store_id  = 0;}
    if (!isset($layoutid)) {$layoutid = 1;}

/* layoutid */
?>

<?php echo $header; ?>

<?php echo (isset($boxed) ? $boxed : ''); ?>
<?php echo $content_top; ?>

<?php if ($layoutid == 3) : ?>
<div class="content separator-section">
    <div class="container">
        <hr>
    </div>
</div>
<?php endif; ?>


    <?php if ($layoutid != 4) : ?>

    <?php if ($layoutid != 2 && $layoutid != 12) : ?>
    <div class="<?php echo ($layoutid != 9 ? 'content' : ''); ?>">
    <?php endif; ?>

        <div class="container">
            <div class="row">
                <?php echo $column_left; ?>

                <?php echo ($layoutid != 2 && $layoutid != 12 ? (isset($left_home) ? $left_home : '') : ''); ?>

                <?php echo ($layoutid == 2 || $layoutid == 12 ? (isset($left_home2) ? $left_home2 : '') : ''); ?>

                <?php echo ($layoutid != 7 ? (isset($promo) ? $promo : '') : ''); ?>

                <?php echo ($layoutid != 2 && $layoutid != 12 ? (isset($right_home) ? $right_home : '') : ''); ?>

                <?php echo ($layoutid == 2|| $layoutid == 12 ? (isset($right_home2) ? $right_home2 : '') : ''); ?>

                <?php echo $column_right; ?>

                <?php echo ($layoutid == 7 ? (isset($promo) ? $promo : '') : ''); ?>

            </div>
        </div>

    <?php if ($layoutid != 2 && $layoutid != 12) : ?>
    </div>
    <?php endif; ?>
<?php echo ($layoutid != 2 && $layoutid != 12 ? $content_bottom : ''); ?>
<?php endif; ?>


<?php echo $footer; ?>