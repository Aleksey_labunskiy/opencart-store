<?php
// HTTP
define('HTTP_SERVER', 'http://opencartstore:8080/admin/');
define('HTTP_CATALOG', 'http://opencartstore:8080/');

// HTTPS
define('HTTPS_SERVER', 'http://opencartstore:8080/admin/');
define('HTTPS_CATALOG', 'http://opencartstore:8080/');

// DIR
define('DIR_APPLICATION', 'D:/OpenServer/domains/opencartstore/admin/');
define('DIR_SYSTEM', 'D:/OpenServer/domains/opencartstore/system/');
define('DIR_IMAGE', 'D:/OpenServer/domains/opencartstore/image/');
define('DIR_LANGUAGE', 'D:/OpenServer/domains/opencartstore/admin/language/');
define('DIR_TEMPLATE', 'D:/OpenServer/domains/opencartstore/admin/view/template/');
define('DIR_CONFIG', 'D:/OpenServer/domains/opencartstore/system/config/');
define('DIR_CACHE', 'D:/OpenServer/domains/opencartstore/system/storage/cache/');
define('DIR_DOWNLOAD', 'D:/OpenServer/domains/opencartstore/system/storage/download/');
define('DIR_LOGS', 'D:/OpenServer/domains/opencartstore/system/storage/logs/');
define('DIR_MODIFICATION', 'D:/OpenServer/domains/opencartstore/system/storage/modification/');
define('DIR_UPLOAD', 'D:/OpenServer/domains/opencartstore/system/storage/upload/');
define('DIR_CATALOG', 'D:/OpenServer/domains/opencartstore/catalog/');

// DB
define('DB_DRIVER', 'mysqli');
define('DB_HOSTNAME', 'localhost');
define('DB_USERNAME', 'root');
define('DB_PASSWORD', '');
define('DB_DATABASE', 'opencartstore');
define('DB_PORT', '3306');
define('DB_PREFIX', 'oc_');
